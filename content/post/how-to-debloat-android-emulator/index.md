+++
author = "Rinav Gangar"
title = "How to debloat Android emulator and remove useless junk"
date = "2021-04-21"
description = "Detox Android emulators by debloting unnecessary apps without root access"
image = "cover.jpg"
tags = ["debloat", "freeup-space", "emulator", "android", "adb", "detox phone"]
keywords = ["debloat", "freeup-space", "emulator", "android", "detox", "adb", "phone"]
aliases = ["free-up-space-on-android-device", "free-up-emulator"]
categories = ["android"]
toc = false
+++

There are tons of useless apps pre-installed on emulators which I never use, nor are of any use to me while developing Android apps. Its safe to either disable them or remove them as they do not add any value to our development cycle.

Another problem is that these apps sync with the Google Server and often slow down emulator. The apps that I work on often have Google Login feature. Once the device is signed in using Google account it starts syncing account details and app data. This is unnecessary usage of emulated cpu which causes slowness and battery drain of laptop.

Take for instance 'Google Photo' app. It's useless to me unless I am developing camera feature or require some gallery feature from the app, or am developing a export feature using existing Google Photos to some third party cloud service.

Another useless app on emulator is GMail, Play Music, Play Movies etc. Why do I need this app on emulator? Unless you do not need any features of these apps it's safe to delete them from emulators gaining up some space on SSD drive.

If you feel deleting app might cause issues you can also disable them for safety. Test it and then uninstall them. I use this method to detox my phone. I disable lots of unneeded bloated apps; use my phone for a day or two and then decide if this caused any errors or so. If all is well I uninstall them.
Remember emulators are temparary devices; you can always factory reset them.

These days the recent size of emulators have gone massive. They go up **twelve+ Gb** in size. Often it is noted that we do not need such a bloated emulator.

Below are the list of commands that will detox emulator and even your mobile phone if you want to try it out on it. Connect via USB if using phone; and run these commands in terminal.

```shell
# To list all the installed packages run this in a terminal
adb shell 'pm list packages'

# To filter by specific packages eg google
adb shell 'pm list packages -f google'

# To uninstall gmail
adb shell 'pm uninstall --user 0 com.google.android.gm'

# To uninstall youtube music and youtube
adb shell 'pm uninstall --user 0 com.google.android.apps.youtube.music'
adb shell 'pm uninstall --user 0 com.google.android.youtube'

# To uninstall play movies
adb shell 'pm uninstall --user 0 com.google.android.videos'

# To uninstall calendar
adb shell 'pm uninstall --user 0 com.google.android.calendar'

# To uninstall photos
adb shell 'pm uninstall --user 0 com.google.android.apps.photos'

# To uninstall Duo
adb shell 'pm uninstall --user 0 com.google.android.apps.tachyon'

# to uninstall maps
adb shell 'pm uninstall --user 0 com.google.android.apps.maps'
```

Instead of uninstalling if you wish to only disable an app then use this command

```shell
# to disable any app change the command with the following one
adb shell 'pm disable-user --user 0 sample-app-name'

# to disable maps
adb shell 'pm disable-user --user 0 com.google.android.apps.maps'
```

Thats it's folks. Hope this information will be useful in your Android development journey and help in tweaking your environment.

###### Photo by [NeONBRAND](https://unsplash.com/@neonbrand?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText")
