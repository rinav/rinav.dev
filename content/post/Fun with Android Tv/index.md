+++
author = "Rinav Gangar"
title = "Customize AndroidTV BrowseFragment screen using leanback library"
date = "2021-05-17"
description = "Develop Android TV app using tvsdk, leanback, rxJava, retrofit, dagger-hilt, tmdb etc"
image = "cover.jpg"
      images = "ss-android-tv-rinav.dev-device-2021-05-17-154853.jpg"
tags = ["androidTV", "leanback", "rxjava", "retrofit", "dagger-hilt", "tmdb", "mvvm", "viewmodel", "shared-viewmodel", "android-tv"]
keywords = ["BrowseFragment", "leanback", "androidTV", "android-tv", "Customize BrowseFragment", "adb", "tmdb"]
aliases = ["customize-browse-fragment", "android-tv-app"]
categories = ["android"]
toc = false
+++

Happy to share a fun weekend project on Android TV; revisiting RxJava and Dagger after a long time.
I build a launch page using BrowseFragment and customized it to my liking.

Used rxJava groupBy operator and retrofit to separate out trending movies and tv shows from a single api.
Used a zip operator to get genres from two api calls for movies and tv shows and used its output to display them on BrowseFragment

I first customized the titleview from BrowseFragment to display title and some metadata. Was successful in implementation but the titleView is not sticky while scrolling. So when you scroll down the title moves up from and cant be seen, so it was useless to use this customize.
However I learnt how easy it was to customize the look and feel of the titleView if thats what you need. After searching and going through documentation and pulling my hair for such a simple task I came across a stackoverflow answer which helped me with the idea.

So as per the post, customized Dashboard with a combination of BrowseFragment and a regular fragment (Yeah!) to display selected item details at top along with backdrop image. Once we have a regular fragment at hand its very easy to use sharedViewModel and display metadata on the TitleFragment.

![Android TV app - SS1](ss-android-tv-rinav.dev-device-2021-05-17-154853.jpg)
![Android TV app - SS2](cover.jpg)

Some new happy observation is that we can now use AppCompatActivity; previously androidTV sdks were really behind android sdks. We had to use FragmentActivity which was a real pain and caused lots of crashes because of theme.

Finally Dagger is easy to use because of Hilt. Had abandoned Dagger completely in favor of Koin. Will try out dagger in the next project I'll work on.

{{< youtube id="3AQMJF-w6BU" title="AndroidTV application using leanback" >}}

And yes, I wonder is the state of Jetpack Compose on an android-tv platform? How soon can we try out compose on tv!

Couple of improvements and next step for this side project

- implement details ui using DetailsSupportFragment to display more metadata and customize it
- implement room to cache data and reduce network calls
- improvise above step and implement workmanager to download and sync local source with remote
- try out leanback-tabs lib, but I do not yet have any usecase for tabs
- research on a api to get item tailers so I can implement exoplayer and media-session video player
- handle rxJava errors
- use leanback-paging

Thanks to TMDB team for a free and open api to developers working on some fun side projects.

Libraries and framework used: #androidTV #leanback #rxJava #dagger-hilt #retrofit #tmdbapi #mvvm #viewmodel
