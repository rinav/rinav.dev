+++
title = "About"
description = "Android | Kotlin | Coroutines | Epoxy | Manjaro | Linux | Git | Voice First | Localization | Amazon Alexa | ZSH | GCP | Loves Yanni"
image = "cover.jpg"
author = "Rinav Gangar"
email = "rinav4all@gmail.com"
aliases = ["about-us", "about", "contact"]
tags = ["about", "contact"]
date = "2020-06-23"
lastmod = "2020-06-23"
toc = false
+++

## Hi, I'm Rinav Gangar

I am leading a team of 8 talented developers at [Agrahyah Technologies Pvt Ltd](https://agrahyah.com) where I am leading development of [aawaz app](https://play.google.com/store/apps/details?id=com.aawaz). Apart from android and Kotlin I love exploring Linux.

With over 9+ years of enterprise in software and mobile development working with a broad range of industries including Audio, News Media, Banking & Mutual Funds and IOT.

My curiosity for android os and Java made me root my first android phone; Samsung Galaxy Note 1 - GT7000. It was massive in size with a magical pen. Rooting and flashing custom ROM was the magical moment when I decided to get into android development.

But what really pushed me into android was the announcement of Android Studio on IntelliJ as I hated eclipse to it's core. I have used every release of android studio, all the canary's and betas.

With more than **six** years of Android development experience, I have learned valuable lessons on software and product development.

In a previous life I used to write code for server-side enterprise Java applications.

When I am not developing apps I like to read on Kindle or take a ride on my bike.

If you want to discuss anything or have any questions, please reach out to me on [`Twitter`](https://twitter.com/_rinav), I am always happy to discuss things!

> Your entire past is over with the sunrise.\
> Your entire future begins today.\
> --- Mahatria

<!-- <span>Photo by <a href="https://unsplash.com/@sigmund?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Sigmund</a> on <a href="https://unsplash.com/?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span> -->

###### Photo by [Sigmund](https://unsplash.com/@sigmund?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
