---
title: "Finance"
description: "Post related to personal finance"
slug: "finance"
image: "category-finance.jpg"
style:
    background: "#2a9d8f"
    color: "#000"
credits: <a href="https://www.freepik.com/vectors/earn">Earn vector created by pch.vector - www.freepik.com</a>
---
